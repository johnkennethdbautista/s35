const express = require('express')
const mongoose = require('mongoose')
require('dotenv').config()
const app = express()
const port = 3000


mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@wdc028-course-booking.ucfa5iz.mongodb.net/users-s35?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
	})

let db = mongoose.connection

db.on('error', () => console.log('Connection Error!'))
db.once('open', () => console.log('Connected to MongoDB!'))

const user_schema = new mongoose.Schema({
	username: String,
	password: String
})

const User = mongoose.model('User', user_schema)

app.use(express.json())
app.use(express.urlencoded({extended: true}))

app.post('/signup', (request, response) => {
	User.findOne({username: request.body.username}).then((result, error) => {
		if(result !== null && result.username == request.body.username){
			return response.send(`Username ${request.body.username} is already existed!`)
		} else {
			let new_user = new User({
				username: request.body.username,
				password: request.body.password
			})

			new_user.save().then((created_user, error) => {
				if(error){
					return console.log(error)
				} else {
				return response.status(201).send(`User ${request.body.username} has been created!`)
			}
			})
		}
	})
})

app.get('/signup',  (request, response) => {
	User.find({}).then((result, error) => {
		if(error){
			return console.log(error)
		}

		return response.status(200).send(result)
	})
})

app.listen(port, () => console.log(`The server is running in localhost: ${port}`))